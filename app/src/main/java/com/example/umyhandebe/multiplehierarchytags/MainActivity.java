package com.example.umyhandebe.multiplehierarchytags;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ArrayList<String> textLista = new ArrayList<String>();
    ArrayList<Tag> tagLista = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textLista.add("text1");
        textLista.add("text2");

        LinearLayout container = (LinearLayout) findViewById(R.id.container);

        for (int i = 0; i< textLista.size(); i++) {
            View view = getLayoutInflater().inflate(R.layout.inflationlayout, container, false);
            TextView textV = (TextView)view.findViewById(R.id.testText);
            textV.setText(textLista.get(i));
            Tag myTag = new Tag();
            tagLista.add(myTag);
            textV.setTag(myTag);
            container.addView(view);
        }

        /*findViewById kommer själv leta i hela hierarkin efter vyn med rätt Id.
        * Men man måste loopa igenom alla children med getChildAt(i) eftersom alla
        * TextViews har samma id (annars hittas bara 1 vy)*/
        for (int i = 0; i < container.getChildCount(); i++){
            TextView txt = (TextView)container.getChildAt(i).findViewById(R.id.testText);
            Log.i("TAG", i+" FINDVIEWBYID " + txt.getText().toString());
        }

        /*findViewWithTag fungerar på samma sätt, men kräver mindre kod i och med
        att man inte behöver ange .getChildAt()
         */
        for (int i = 0; i < container.getChildCount(); i++){
            TextView tw = (TextView)container.findViewWithTag(tagLista.get(i));
            tw.setText("test");     //här kan vi byta ut innehållet
            Log.i("TAG", i + " FINDVIEWWITHTAG "+tw.getText().toString());
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
